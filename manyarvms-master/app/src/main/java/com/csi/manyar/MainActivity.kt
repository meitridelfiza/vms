package com.csi.manyar

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import android.widget.Toast
import com.csi.manyar.utils.SharedPreference

class MainActivity : AppCompatActivity() {

    private lateinit var btnLogin: Button
    private lateinit var btnRegister: Button
    private lateinit var btnShowPassword: CheckBox

    private lateinit var email: EditText
    private lateinit var password: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val MySharedPreference: SharedPreference = SharedPreference(this)

        /*
            CHECK USERS LOGIN OR NOT IN  SHARED PREFERENCE
            IF NOT LOGIN DIRECT TO LOGIN ACTIVITY
         */

        btnLogin = findViewById(R.id.btnLogin)
        email = findViewById(R.id.username)
        password = findViewById(R.id.password)


        btnLogin.setOnClickListener {
            val email = email.text.toString().trim()
            val password = password.text.toString().trim()

            print(email)

            if(email.isEmpty()){
                Toast.makeText(applicationContext, "Isi Email", Toast.LENGTH_SHORT).show()
            }

        }
    }
}
